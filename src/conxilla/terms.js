dojo.provide("conxilla.terms");

conxilla.terms.nsLayout = "http://conzilla.org/terms/layout#";
conxilla.terms.ns2 = "http://conzilla.org/terms/style#";

conxilla.terms.priority = conxilla.terms.nsLayout + "priority";
conxilla.terms.layoutIn = conxilla.terms.nsLayout + "layoutIn";
conxilla.terms.layerIn = conxilla.terms.nsLayout + "layerIn";
conxilla.terms.path = conxilla.terms.nsLayout + "path";
conxilla.terms.box = conxilla.terms.nsLayout + "box";
conxilla.terms.type = conxilla.terms.nsLayout + "type";
conxilla.terms.style = conxilla.terms.nsLayout + "style";

conxilla.terms.XSD = {
	Float: "http://www.w3.org/2001/XMLSchema#float"
};