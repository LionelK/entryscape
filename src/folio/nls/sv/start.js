/*
 * Copyright (c) 2007-2010
 *
 * This file is part of Confolio.
 *
 * Confolio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Confolio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Confolio. If not, see <http://www.gnu.org/licenses/>.
 */

({"welcomeMessage": "Platformen som ger dig snabb och enkel tillgång personliga och delade portföljer från varhelst du befinner dig. Ladda upp filer, länka till material på webben som videos, foton, föreläsningar eller skapa material direkt i din portfölj. Organisera sen ditt material i kataloger och om du så behöver kan du beskriva det med hjälp av etablerade metadata standarder.",
  "welcomeHeader": "Välkommen till Confolio!",
  "searchLabel": "Sök efter portföljer",
  "latestMaterial": "Senaste materialet"
})
