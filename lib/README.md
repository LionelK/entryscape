# EntryScape external libraries

## Dojo - The Dojo tookit
We currently rely on version 1.8.3 of the [Dojo toolkit](http://dojotoolkit.org/).
The following script downloads dojo source, unpacks and builds a version that is suitable for development and
running EntryScape directly from source.

    ./INSTALL-dojo.sh

## SWFObject
We rely on [SWFObject version 2.2](http://code.google.com/p/swfobject/) for embedding flash files.

## Standalone WordPress Audio Player
We use the standalone WordPress [audio player](http://wpaudioplayer.com/standalone/) to provide a inline player
for mp3 files.

## Date Format 1.2.3
A small utility library for converting between various date formats. Not used in the main application of EntryScape.
TODO: Check if it is really needed or if it can be excluded from the repository.


